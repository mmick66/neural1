//
//  ViewController.swift
//  Neural1
//
//  Created by Michael Michailidis on 03/11/2016.
//  Copyright © 2016 Michael Michailidis. All rights reserved.
//



import UIKit

extension UIColor {
    class var on: UIColor {
        get{
           return UIColor.init(colorLiteralRed: 66.0/255.0, green: 34.0/255.0, blue: 56.0/255.0, alpha: 1.0)
        }
    }
    class var off: UIColor {
        get{
            return UIColor.init(colorLiteralRed: 228.0/255.0, green: 179.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        }
    }
}

class DrawingViewController: UIViewController {
    
    var network:Network?

    @IBOutlet weak var guessedDigitLabel: UILabel!
    @IBOutlet weak var digitView: DigitView!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // UI
        let dragGesture = UIPanGestureRecognizer(target: self, action: #selector(DrawingViewController.dragHappened))
        self.digitView.addGestureRecognizer(dragGesture)
        
        self.guessedDigitLabel.text = ""
        
        
        self.digitView.backgroundColor = UIColor.clear
        self.clear()
        
    }
    
    
    func dragHappened(recogniser : UIPanGestureRecognizer) {
        
        let point = recogniser.location(in: self.digitView)
        
        switch recogniser.state {
            
        case .began:
            guessedDigitLabel.text = ""
            draw(at: point)
            break;
            
            
        case .changed:
            draw(at: point)
            break;
            
            
        case .ended:
            compute()
            break;
            
        default:
            break
        }
        
        
    }
    
    func clear() {
        
        for sview in self.digitView.subviews {
            sview.backgroundColor = UIColor.off
        }

    }
    
    
    func compute() {
        
        var serialized = Array(repeating: 0.0, count: DigitView.GRID*DigitView.GRID)
        
        for s in self.digitView.subviews {
            if s.backgroundColor == UIColor.on {
                
                serialized[s.tag] = 1.0
                
            }
        }
        
        guard let net = self.network else {
            return
        }
        
        let results = net.predict(input: serialized)
        
        var bestg = 0.0
        var digit = 0
        for (i,p) in results.enumerated() {
            if p > bestg {
                bestg = p
                digit = i
            }
        }
        
        
        print("Found => \(results)")
        
        self.guessedDigitLabel.text = String(digit)
        
        
        let animateOut:()->Void = {
            self.guessedDigitLabel.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
         
        }
        let animateIn = {
            self.guessedDigitLabel.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        }
        
        UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: animateOut, completion: { finished in
            UIView.animate(withDuration: 0.1, animations: animateIn, completion: { finished in
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                    self.clear()
                }
                
            })
        })
        
        
        
    }
    
    func draw(at point: CGPoint) {
       
        let touched = self.digitView.getSquares(at: point)
        
        for square in touched {
            
            square.backgroundColor = UIColor.on
        }
        
    }

}

