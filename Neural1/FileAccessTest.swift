//
//  FileAccessTest.swift
//  Neural1
//
//  Created by Michael Michailidis on 18/11/2016.
//  Copyright © 2016 Michael Michailidis. All rights reserved.
//

import XCTest

class FileAccessTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testFileLoading() {
        
        guard let path = Bundle.main.path(forResource: "simple.training.set", ofType: "cv") else {
            return
        }
        
        guard let reader = Reader(path:path) else {
            return
        }
        
        let parser = Parser(with: reader, width: 5, height: 5)
        
        
        let set:TrainingSet = try! parser.parse(off: ".", on: "@")
        
        XCTAssert(set.count == 10, "")
    }
    
    func testPerformanceExample() {
        self.measure {
        }
    }
    
}
