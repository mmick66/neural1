//
//  TrainingViewController.swift
//  Neural1
//
//  Created by Michael Michailidis on 24/11/2016.
//  Copyright © 2016 Michael Michailidis. All rights reserved.
//

import UIKit

class TrainingViewController: UIViewController {
    
    var network:Network?
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.loader.startAnimating()
        DispatchQueue.global(qos: .background).async {
            
            guard let set = self.loadData(name: "simple.training.set") else {
                return
            }
            
            let net = Network(inputLayerCount: 25, hiddenLayerCount: 5, outputLayerCount: 10)
            
            net.train(set: set)
            
            self.network = net
            
            DispatchQueue.main.async {
                self.loader.stopAnimating()
                
                self.performSegue(withIdentifier: "GoToDigitsView", sender: self)
                
            }
        }
        
        
    }
    
    func loadData(name:String) -> TrainingSet? {
        
        guard let path = Bundle.main.path(forResource: name, ofType: "cv") else {
            return nil
        }
        
        guard let reader = Reader(path:path) else {
            return nil
        }
        
        let parser = Parser(with: reader, width: 5, height: 5)
        
        let set:TrainingSet = try! parser.parse(off: ".", on: "@")
        
        return set
    }
    


    
    // MARK: - Navigation

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "GoToDigitsView" {
            
            let vc: DrawingViewController = segue.destination as! DrawingViewController
            vc.network = self.network
            
        }
    }


}
