//
//  Network.swift
//  Neural1
//
//  Created by Michael Michailidis on 16/11/2016.
//  Copyright © 2016 Michael Michailidis. All rights reserved.
//



import Foundation

typealias Vector = [Double]

infix operator **

func **(left:Vector, right:Vector) -> Double {
    precondition(left.count == right.count)
    var sum = 0.0
    for (l, r) in zip(left, right) {
        sum += l * r
    }
    return sum
}


func sigmoid(t:Double, deriv:Bool=false) -> Double {
    if deriv == true {
        return t * (1 - t)
    }
    return 1.0 / (1.0 + exp(-t))
}

class Neuron {
    
    var weights:[Double] = []
    var bias:Double
    
    
    
    init(weights:[Double], bias:Double=1.0) {
        self.weights = weights
        self.bias = bias
    }
    
    func output(input:[Double]) -> Double {
        
        precondition(input.count == weights.count, "The input vector is diffent size from the weights")
        
        let sum = input ** weights
        return sigmoid(t: sum)
    }
    
}

struct Layers:Sequence, IteratorProtocol {
    var hidden:Layer
    var output:Layer
    
    typealias Element = Layer
    init(hidden:Layer, output:Layer) {
        self.hidden = hidden
        self.output = output
    }
    var index = 0
    mutating func next() -> Element? {
        let ret:Element?
        switch index {
        case 0:
            ret = self.hidden
        case 1:
            ret = self.output
        default:
            ret = nil
            
        }
        index += 1
        return ret
    }
}

struct Result {
    var hidden:Vector
    var output:Vector
    
}

typealias Layer = [Neuron]

class Network {
    
    var layers : Layers
    
    static let TrainingIterations:Int = 10000
    init(inputLayerCount:Int, hiddenLayerCount:Int, outputLayerCount:Int, bias:Double=1.0) {
        
        var hidden_layer:[Neuron] = [Neuron]()
        for _ in 0..<hiddenLayerCount {
            
            var weights = [Double]()
            
            for _ in 0..<(inputLayerCount+1) {
                
                let ran = Double(arc4random()) / Double(UINT32_MAX)
                weights.append(ran)
            }
            let neuron = Neuron(weights: weights, bias:bias)
            hidden_layer.append(neuron)
        }
        
        var output_layer:[Neuron] = [Neuron]()
        for _ in 0..<outputLayerCount {
            
            var weights = [Double]()
            
            for _ in 0..<(hiddenLayerCount+1) {
                
                let ran = Double(arc4random()) / Double(UINT32_MAX)
                weights.append(ran)
            }
            let neuron = Neuron(weights: weights, bias:bias)
            output_layer.append(neuron)
        }
        
        self.layers = Layers(hidden: hidden_layer, output: output_layer)
        
    }
    
    init(layers: Layers) {
        self.layers = layers
    }
    
    func train(set:TrainingSet) {
        
        for _ in 0..<Network.TrainingIterations {
            
            for (input, target) in set {
                
                self.backward(inputs: input, targets: target)
                
            }
        }
    }
    
    func forward(input:Vector) -> Result {
        
        var inputs:Vector = input
        
        var total:[Vector] = [Vector]()
        
        for layer in layers {
            
            var outputs = [Double]()
            for neuron in layer {
                
                let biased = inputs + [neuron.bias]
                
                let o = neuron.output(input: biased)
           
                outputs.append(o)
            }
            
            total.append(outputs)
            
            inputs = outputs
        }
        
        return Result(hidden: total.first!, output: total.last!)
    }
    
    func predict(input:[Double]) -> [Double] {
        return forward(input: input).output.map({ (x) -> Double in
            return Double(round(100*x)/100)
        })
    }
    
    
    func backward(inputs:Vector, targets:Vector) {
        
        
        let result = self.forward(input: inputs)
        
        
        var output_deltas = [Double]()
        for (output, target) in zip(result.output, targets) {
            
            let d = sigmoid(t: output, deriv: true) * (output - target)
            output_deltas.append(d)
            
        }
        
        for (i,neuron) in self.layers.output.enumerated() {
            
            for (j,houtput) in (result.hidden + [1.0]).enumerated() {
                
                neuron.weights[j] -= output_deltas[i] * houtput
                
            }
        }
        
        
        var hidden_deltas = [Double]()
        
        for (i, hout) in result.hidden.enumerated() {
            
            var weights = [Double]()
            for neuron in self.layers.output {
                let w = neuron.weights[i]
                weights.append(w)
            }
            
            
            let dot = output_deltas ** weights
            let r = sigmoid(t: hout, deriv: true) * dot
            hidden_deltas.append(r)
        }
        
        for (i, hidden_neuron) in self.layers.hidden.enumerated() {
            
            for (j, input) in (inputs + [1.0]).enumerated() {
                
                hidden_neuron.weights[j] -= hidden_deltas[i] * input
                
            }
            
        }
        
    }

}



