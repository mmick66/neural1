//
//  MatrixTests.swift
//  Neural1
//
//  Created by Michael Michailidis on 17/11/2016.
//  Copyright © 2016 Michael Michailidis. All rights reserved.
//

import XCTest

class MatrixTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testBasic() {
        
        let m1: Matrix = Matrix([5.0, 6.0, 2.0, 1.0], rows:2, columns:2)
        XCTAssert(m1[0,0] == 5.0, "1x1 Matrix wrong")
        
        print(m1)
    }
    

    func testInnerProduct() {
        
        let v1:Vector = [0.3, 3.2, 2.1]
        let v2:Vector = [0.5, 2.1, 5.0]
        
        XCTAssert((v1 ** v2) == 17.37, "Wrong result")
      
    }
    func testMultiplication() {
        
        var dot:Matrix
        
        dot = Matrix([2, 2], rows:1, columns:2) * Matrix([1, 1], rows:1, columns:2)
        XCTAssert(dot[0,0] == 4.0, "1x1 Matrix wrong")
        
        dot = Matrix([2, 2], rows:1, columns:2) * Matrix([2, 3], rows:1, columns:2)
        XCTAssert(dot[0,0] == 10.0, "1x1 Matrix wrong")
        
        dot = Matrix([2, 2, 5], rows:1, columns:3) * Matrix([1, 2, 3], rows:1, columns:3)
        XCTAssert(dot[0,0] == 21.0, "1x1 Matrix wrong")
        
        dot = Matrix([2, 2, 5], rows:1, columns:3) * Matrix([1, 2, 3], rows:1, columns:3)
        XCTAssert(dot[0,0] == 21.0, "1x1 Matrix wrong")
        
        dot = Matrix([4, 6, 2, 1, 3, 2], rows:2, columns:3) * Matrix([4, 4, 7, 3, 9, 1], rows:3, columns:2)
        XCTAssert(dot.rows == 2 && dot.columns == 2, "Matrix result is of wrong size")
        XCTAssert(dot == Matrix([76, 36, 43, 15], rows:2, columns:2), "Matrix result is wrong")
        
        
    }
    
    func testTransposition() {
        
        var trans:Matrix
        
        trans = Matrix([1, 2], rows:1, columns:2)
        
        XCTAssert(trans^ == Matrix([1, 2], rows:2, columns:1), "Matrix did not transpose correctly")
        
        trans = Matrix([1, 2, 3, 4], rows:2, columns:2)
        
        XCTAssert(trans^ == Matrix([1, 3, 2, 4], rows:2, columns:2), "Matrix did not transpose correctly")
        
        trans = Matrix([1, 2, 3, 4, 5, 6], rows:3, columns:2)
        
        XCTAssert(trans^ == Matrix([1, 3, 5, 2, 4, 6], rows:2, columns:3), "Matrix did not transpose correctly")
    }
    
    
    func testAddition() {
        
        var sum:Matrix
        
        sum = Matrix([2, 2], rows:1, columns:2) + Matrix([1, 1], rows:1, columns:2)
        
        XCTAssert(sum == Matrix([3, 3], rows:1, columns:2), "1x1 Matrix wrong")
        
        sum = Matrix([1, 2, 3, 4], rows:2, columns:2) + Matrix([2, 3, 4, 5], rows:2, columns:2)
        
        XCTAssert(sum == Matrix([3, 5, 7, 9], rows:2, columns:2), "1x1 Matrix wrong")
    }
    
    
}




    
