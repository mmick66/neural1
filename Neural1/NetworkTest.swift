//
//  NetworkTest.swift
//  Neural1
//
//  Created by Michael Michailidis on 17/11/2016.
//  Copyright © 2016 Michael Michailidis. All rights reserved.
//

import XCTest

class NetworkTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testSimpleXOR() {
        
        let AND     = Neuron(weights: [20, 20, -30], bias:1.0)
        let OR      = Neuron(weights: [20, 20, -10], bias:1.0)
        let NAND    = Neuron(weights: [-60, 60, -30], bias:1.0)
        
        let lrs = Layers(hidden: [AND, OR], output: [NAND])
        
        let net = Network(layers: lrs)
        
        let solutions:[String:Double] = ["0,0":9.3831466830067595e-14,
                                         "0,1":0.9999999999999059,
                                         "1,0":0.9999999999999059,
                                         "1,1":9.383146683006828e-14]
        
        for x in [0.0, 1.0] {
            
            for y in [0.0, 1.0] {
                
                let outputs = net.forward(input: [x, y])
                let key = String(format: "%i,%i", Int(x), Int(y))
                
                
                XCTAssert(outputs.1.first == solutions[key]!, "")
                
            }
        }
        
    }
   
    func testSigmoid() {
        
        func sigmoid(t:Double) -> Double {
            return 1.0 / (1.0 + exp(-t))
        }
        
        for x in -100..<100 {
            
            let output = sigmoid(t: Double(x))
            print("\(x) => \(output)")
        }
    }
    
    func testSmallSetPredict() {
        
        let net = Network(inputLayerCount: 25, hiddenLayerCount: 5, outputLayerCount: 10)
        
        // Open File
        guard let path = Bundle.main.path(forResource: "simple.training.set", ofType: "cv") else {
            return
        }
        
        guard let reader = Reader(path:path) else {
            return
        }
        
        let parser = Parser(with: reader, width: 5, height: 5)
        
        let set:TrainingSet = try! parser.parse(off: ".", on: "@")
        
        net.train(set: set)
        
        
        
        let prediction0 = net.predict(input: [1.0,1.0,1.0,1.0,1.0,
                                              1.0,0.0,0.0,0.0,1.0,
                                              1.0,0.0,0.0,0.0,1.0,
                                              1.0,0.0,0.0,0.0,1.0,
                                              1.0,1.0,1.0,1.0,1.0])
        
        print("Predict for 0 (original): \(prediction0)")
        
        
        let prediction1 = net.predict(input: [0.0,0.0,1.0,0.0,0.0,
                                              0.0,0.0,1.0,0.0,0.0,
                                              0.0,0.0,1.0,0.0,0.0,
                                              0.0,0.0,1.0,0.0,0.0,
                                              0.0,0.0,1.0,0.0,0.0])
        
        print("Predict for 1 (original): \(prediction1)")
        
        
        let prediction2 = net.predict(input: [1.0,1.0,1.0,1.0,1.0,
                                              0.0,0.0,0.0,0.0,1.0,
                                              1.0,1.0,1.0,1.0,1.0,
                                              1.0,0.0,0.0,0.0,0.0,
                                              1.0,1.0,1.0,1.0,1.0])
        
        print("Predict for 2 (original): \(prediction2)")
        
        
        
        let prediction3 = net.predict(input: [1.0,1.0,1.0,1.0,1.0,
                                              0.0,0.0,0.0,0.0,1.0,
                                              0.0,1.0,1.0,1.0,1.0,
                                              0.0,0.0,0.0,0.0,1.0,
                                              1.0,1.0,1.0,1.0,1.0])
        
        print("Predict for 3 (original): \(prediction3)")
        
        
        let prediction4 = net.predict(input: [1.0,0.0,0.0,0.0,1.0,
                                              1.0,0.0,0.0,0.0,1.0,
                                              1.0,1.0,1.0,1.0,1.0,
                                              0.0,0.0,0.0,0.0,1.0,
                                              0.0,0.0,0.0,0.0,1.0])
        
        print("Predict for 4 (original): \(prediction4)")
        
        
        let prediction5 = net.predict(input: [1.0,1.0,1.0,1.0,1.0,
                                              1.0,0.0,0.0,0.0,0.0,
                                              1.0,1.0,1.0,1.0,1.0,
                                              0.0,0.0,0.0,0.0,1.0,
                                              1.0,1.0,1.0,1.0,1.0])
        
        print("Predict for 5 (original): \(prediction5)")
        
        
        let prediction6 = net.predict(input: [1.0,0.0,0.0,0.0,0.0,
                                              1.0,0.0,0.0,0.0,0.0,
                                              1.0,1.0,1.0,1.0,1.0,
                                              1.0,0.0,0.0,0.0,1.0,
                                              1.0,1.0,1.0,1.0,1.0])
        
        print("Predict for 6 (original): \(prediction6)")
        
        
        let prediction7 = net.predict(input: [1.0,1.0,1.0,1.0,1.0,
                                              0.0,0.0,0.0,0.0,1.0,
                                              0.0,0.0,0.0,0.0,1.0,
                                              0.0,0.0,0.0,0.0,1.0,
                                              0.0,0.0,0.0,0.0,1.0])
        
        print("Predict for 6 (original): \(prediction7)")
        
        
        let prediction8 = net.predict(input: [1.0,1.0,1.0,1.0,1.0,
                                              1.0,0.0,0.0,0.0,1.0,
                                              1.0,1.0,1.0,1.0,1.0,
                                              1.0,0.0,0.0,0.0,1.0,
                                              1.0,1.0,1.0,1.0,1.0])
        
        print("Predict for 8 (original): \(prediction8)")
        
        
        let prediction9 = net.predict(input: [1.0,1.0,1.0,1.0,1.0,
                                              1.0,0.0,0.0,0.0,1.0,
                                              1.0,1.0,1.0,1.0,1.0,
                                              0.0,0.0,0.0,0.0,1.0,
                                              0.0,0.0,0.0,0.0,1.0])
        
        print("Predict for 9 (original): \(prediction9)")
        
        
        
        let prediction3p = net.predict(input: [0.0,1.0,1.0,1.0,0.0,
                                               0.0,0.0,0.0,1.0,1.0,
                                               0.0,0.0,1.0,1.0,0.0,
                                               0.0,0.0,0.0,1.0,1.0,
                                               0.0,1.0,1.0,1.0,0.0])
        
        print("Predict for 3: \(prediction3p)")
        
    
        
        let prediction2p = net.predict(input: [0.0,1.0,1.0,1.0,0.0,
                                               0.0,0.0,0.0,1.0,1.0,
                                               0.0,0.0,1.0,1.0,0.0,
                                               0.0,0.0,0.0,1.0,1.0,
                                               0.0,1.0,1.0,1.0,0.0])
        
        print("Predict for 8: \(prediction2p)")
        
    }
    
    func testPerformance() {
        
        self.measure {
            
            
            
        }
    }
    
}
