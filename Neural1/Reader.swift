//
//  Reader.swift
//  Neural1


// Martin R @ http://stackoverflow.com/a/24648951/1389383

import Foundation

enum ReaderError: Error {
    case CannotReadFile
}

class Reader : Sequence, IteratorProtocol {
    
    typealias Element = String

    
    var buffer : Data
    
    var handler : FileHandle!
    
    var delim : Data
    
    var end : Bool = false
    
    let encoding : String.Encoding
    let chunkSize : Int
    
    init?(path: String, delimiter: String = "\n", encoding : String.Encoding = String.Encoding.utf8, chunkSize : Int = 4096) {
        
        self.chunkSize = chunkSize
        self.encoding = encoding
        
        guard   let fhandler = FileHandle(forReadingAtPath: path),
                let dlimiter = delimiter.data(using: encoding) else {
                
                return nil
        }
        
        self.handler    = fhandler
        self.delim      = dlimiter
        self.buffer     = Data(capacity: chunkSize)
    }
    
    func next() -> String? {
        
        precondition(self.handler != nil, "Attempt to read from closed file")
        
        while !self.end {
            
            if let range = buffer.range(of: delim) {
                
                let line = String(data: self.buffer.subdata(in: 0..<range.lowerBound), encoding: encoding)
                self.buffer.removeSubrange(0..<range.upperBound)
                
                return line
            }
            
            let read = handler.readData(ofLength: self.chunkSize)
            
            if read.count > 0 {
                
                self.buffer.append(read)
                
            } else {
                
                self.end = true
                
                if buffer.count > 0 {
                    
                    let last = String(data: buffer as Data, encoding: encoding)
                    self.buffer.count = 0
                    return last
                }
            }
        }
        
        return nil
        
    }
    
    
    
    func close() -> Void {
        self.handler?.closeFile()
        self.handler = nil
    }
    
    deinit {
        self.close()
    }
}
