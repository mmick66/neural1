//
//  Parser.swift
//  Neural1
//
//  Created by Michael Michailidis on 18/11/2016.
//  Copyright © 2016 Michael Michailidis. All rights reserved.
//

import Foundation

enum ParserError: Error {
    case UnknownCharacter(character:Character)
}

extension String {
    subscript(i: Int)-> Character {
        let i = self.index(self.startIndex, offsetBy: i)
        return self.characters[i]
    }
}

class Parser {
    
    var reader:Reader
    var width:Int
    var height:Int
    
    init(with reader:Reader, width:Int, height:Int) {
        
        self.reader = reader
        self.width = width
        self.height = height
        
    }
    
    
    func parse(off:Character="0", on:Character="1") throws -> TrainingSet {
        
        var trainingSet = TrainingSet(size:width*height)
        
        var characterVector = Vector()
        
        var ln = 0
        for line in self.reader {
            
            if ln == height {
                
                trainingSet.add(input: characterVector, target: line[1])
                
                characterVector = Vector()
                
                ln = 0
                
            }
            else {
                
                for character in line.characters {
                    
                    var value:Double
                    if character == on {
                        value = 1.0
                    }
                    else if character == off {
                        value = 0.0
                    }
                    else {
                        throw ParserError.UnknownCharacter(character:character)
                    }
                    
                    characterVector.append(value)
                    
                }
                
                ln += 1
                
            }
            
        }
        
        return trainingSet
    }

}
