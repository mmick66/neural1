//
//  DigitView.swift
//  Neural1
//
//  Created by Michael Michailidis on 15/11/2016.
//  Copyright © 2016 Michael Michailidis. All rights reserved.
//


import UIKit


class DigitView: UIView {
    
    static let GRID = 5

    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.createGrid()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.createGrid()
    }
    
    private func createGrid() {
        
        for row in 0..<DigitView.GRID {
            
            for col in 0..<DigitView.GRID {
                
                let square = UIView()
                square.tag = row*DigitView.GRID+col
                self.addSubview(square)
                
            }
        }
        
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        let w = self.bounds.size.width / CGFloat(DigitView.GRID)
        let h = self.bounds.size.height / CGFloat(DigitView.GRID)
        
        var fr = CGRect(x:0, y:0, width:w, height:h)
        
        for row in 0..<DigitView.GRID {
            
            for col in 0..<DigitView.GRID {
                
                let square = self.subviews[(row*DigitView.GRID+col)]
                square.frame = fr
                
                fr.origin.x += fr.size.width;
                
            }
            
            fr.origin.x = 0
            fr.origin.y += fr.size.height
        }
        
    }
    
    // MARK: API
    
    func getSquares(at point: CGPoint) -> [UIView] {
        
        return self.subviews.filter({ (sv) -> Bool in
            
            return sv.frame.contains(point)
            
            
        })
    }

    func getSquares(at point: CGPoint, radius : CGFloat) -> [UIView] {
        
        return self.subviews.filter({ (sv) -> Bool in
            
            let dx = sv.frame.midX - point.x
            let dy = sv.frame.midY - point.y
            
            if radius * radius >= dx * dx + dy * dy {
                return true
            }

            
            return false
            
        })
        
    }

}
