//
//  TraningSet.swift
//  Neural1
//
//  Created by Michael Michailidis on 21/11/2016.
//  Copyright © 2016 Michael Michailidis. All rights reserved.
//

import UIKit

struct TrainingSet: Sequence, IteratorProtocol {
    
    typealias Element = (Vector,Vector)
    
    private var data:[Element]
    var size:Int
    
    var count: Int {
        
        get {
            return self.data.count
        }
    }
    
    
    
    var targets:[Vector]
    
    init(size:Int) {
        
        self.data = [Element]()
        self.size = size
        
        // create targets
        self.targets = [[Double]]()
        
        for i in 0..<10 {
            var ar:Vector = Array(repeating: 0.0, count: 10)
            ar[i] = 1.0
            self.targets.append(ar)
        }
    }
    
    mutating func add(input:Vector, target:Character) {
        
        if let i:Int = Int(String(target)) {
            
            let target = self.targets[i]
            let tuple = (input, target)
            self.data.append(tuple)
            
        }
        
        
    }
    
    private var index:Int = 0
    mutating func next() -> Element? {
        
        if index < self.data.count {
            let p = self.data[index]
            index += 1
            return p
        }
        
        return nil
        
    }
}
